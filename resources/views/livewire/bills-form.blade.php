<div>
  <x-app-layout>
    <x-slot name="header">
      <div class="inline-flex justify-between w-full">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          <a href="{{ route('facturas') }}">
            Facturas
          </a>
        </h2>
      @if ($product->id)
        <div>
          <button wire:click="destroy" class="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: red; border-color: red;">
            Eliminar
          </button>
          <a
            class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: green; border-color: green;"
            href="{{ route('facturas.new') }}">
            Nuevo
          </a>
        </div>
      @endif
      </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <form wire:submit.prevent="save" class="py-10 px-4">
                <div>
                    <x-jet-label value="Cliente" />
                    @if ($product->id)
                      <x-jet-input class="block mt-1 w-full" type="text" wire:model="product.client" readonly />
                    @else
                    <x-jet-input class="block mt-1 w-full" type="text" wire:model="product.client" autofocus />
                    @endif
                    @error('product.client') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Tipo de factura" />
                  @if ($product->id)
                    <x-jet-input class="block mt-1 w-full" wire:model="product.type" type="text" readonly />
                  @else
                    <x-jet-input class="block mt-1 w-full" wire:model="product.type" type="text" />
                  @endif
                  @error('product.type') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                @if ($product->id)
                  <div class="mt-4">
                    <x-jet-label value="Total" />
                    <x-jet-input class="block mt-1 w-full" value="{{ $product->total }}" type="text" readonly />
                  </div>

                  <div class="mt-4">
                    <x-jet-label value="Total Con Iva" />
                    <x-jet-input class="block mt-1 w-full" value="{{ $product->total_iva }}" type="text" readonly />
                  </div>
                @endif

                @unless ($product->id)
                <div class="flex items-center justify-end mt-4">
                  <div
                    class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: blue; border-color: blue;" wire:click="addProduct">
                      Agregar producto
                  </div>
                </div>
                @endunless

                @foreach ($details as $i => $detail)
                  <div class="mt-4">
                    <x-jet-label value="Producto" />
                    @if ($details[$i]['id'])
                    <select class="block mt-1 w-full" wire:model="details.{{ $i }}.product_id" disabled>
                      @foreach ($products as $itype)
                        <option value="{{ $itype->id }}">{{ $itype->name }}</option>
                      @endforeach
                    </select>
                    @else
                    <select class="block mt-1 w-full" wire:model="details.{{ $i }}.product_id">
                      @foreach ($products as $itype)
                        <option value="{{ $itype->id }}">{{ $itype->name }}</option>
                      @endforeach
                    </select>
                    @endif
                  </div>

                  <div class="mt-4">
                    <x-jet-label value="Cantidad" />
                    @if ($details[$i]['id'])
                      <x-jet-input class="block mt-1 w-full" value="{{ $details[$i]['qty']}}" type="number" />
                    @else
                      <x-jet-input class="block mt-1 w-full" wire:model="details.{{ $i }}.qty" type="number" />
                    @endif
                  </div>
                @endforeach

                @unless ($product->id != null)
                  <div class="flex items-center justify-end mt-4">
                    <x-jet-button class="ml-4">
                        Guardar
                    </x-jet-button>
                  </div>
                @endunless
              </form>
            </div>
        </div>
    </div>
  </x-app-layout>
</div>
