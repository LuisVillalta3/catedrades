<div>
  <x-app-layout>
    <x-slot name="header">
      <div class="inline-flex justify-between w-full">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Facturas
        </h2>
        <a
          class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: green; border-color: green;"
          href="{{ route('facturas.new') }}">
          Nuevo
        </a>
      </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <table class="table-auto w-full">
                <thead>
                  <tr>
                    <th class="px-4 py-2">Fecha</th>
                    <th class="px-4 py-2">Cliente</th>
                    <th class="px-4 py-2">Total</th>
                    <th class="px-4 py-2">Total Iva</th>
                    <th class="px-4 py-2">Usuario</th>
                    <th>Acciones</th>
                  </tr>
                  <tr>
                    <th class="px-4 py-2">
                    </th>
                    <th class="px-4 py-2">
                      <x-jet-input wire:model="name" class="block mt-1 w-full" type="text" />
                    </th>
                    <th class="px-4 py-2">
                      {{-- <x-jet-input class="block mt-1 w-full" type="text" name="lastname" :value="old('lastname')" /> --}}
                    </th>
                    <th class="px-4 py-2">
                      {{-- <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')"/> --}}
                    </th>
                    <th class="px-4 py-2">&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $item)
                    <tr>
                      <td class="border px-4 py-2">{{ $item->created_at->format('d/m/Y') }}</td>
                      <td class="border px-4 py-2">{{ $item->client }}</td>
                      <td class="border px-4 py-2">{{ $item->total_iva }}</td>
                      <td class="border px-4 py-2">{{ $item->total_iva }}</td>
                      <td class="border px-4 py-2">{{ $item->user->name }}</td>
                      <td class="border px-4 py-2 inline-flex justify-between">
                        <button wire:click="destroy({{ $item->id }})" class="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: red; border-color: red;">
                          Eliminar
                        </button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $users->links() }}
            </div>
        </div>
    </div>
  </x-app-layout>
</div>
