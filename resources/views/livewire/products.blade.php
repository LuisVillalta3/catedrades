<div>
  <x-app-layout>
    <x-slot name="header">
      <div class="inline-flex justify-between w-full">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Productos
        </h2>
        <a
          class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: green; border-color: green;"
          href="{{ route('productos.new') }}">
          Nuevo
        </a>
      </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <table class="table-auto w-full">
                <thead>
                  <tr>
                    <th class="px-4 py-2">Nombre</th>
                    <th class="px-4 py-2">Precio de compra</th>
                    <th class="px-4 py-2">Precio de venta</th>
                    <th class="px-4 py-2">Inventario</th>
                    <th class="px-4 py-2">Estado</th>
                    <th class="px-4 py-2">Proveedor</th>
                    <th class="px-4 py-2">Bodega</th>
                    <th>Acciones</th>
                  </tr>
                  <tr>
                    <th class="px-4 py-2">
                      <x-jet-input wire:model="name" class="block mt-1 w-full" type="text" />
                    </th>
                    <th class="px-4 py-2">&nbsp;</th>
                    <th class="px-4 py-2">&nbsp;</th>
                    <th class="px-4 py-2">&nbsp;</th>
                    <th class="px-4 py-2">&nbsp;</th>
                    <th class="px-4 py-2">&nbsp;</th>
                    <th class="px-4 py-2">&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $item)
                    <tr>
                      <td class="border px-4 py-2">{{ $item->name }}</td>
                      <td class="border px-4 py-2">{{ $item->bought_price }}</td>
                      <td class="border px-4 py-2">{{ $item->sell_price }}</td>
                      <td class="border px-4 py-2">{{ $item->stock }}</td>
                      <td class="border px-4 py-2">{{ $item->state }}</td>
                      <td class="border px-4 py-2">{{ $item->provider }}</td>
                      <td class="border px-4 py-2">{{ $item->cellar->name }}</td>
                      <td class="border px-4 py-2 inline-flex justify-between">
                        <x-jet-button class="ml-2" wire:click="showupdateStock({{ $item->id }})">
                          Actualizar inventario
                        </x-jet-danger-button>
                        <a
                          href="{{ route('productos.edit', $item->id) }}"
                          class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: blue; border-color: blue;">
                          Editar
                        </a>
                        <button wire:click="destroy({{ $item->id }})" class="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: red; border-color: red;">
                          Eliminar
                        </button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              {{ $users->links() }}
            </div>
        </div>
    </div>
  </x-app-layout>
  <x-jet-confirmation-modal wire:model="confirmingUserDeletion">
    <x-slot name="title">
      Actualizar inventario
    </x-slot>

    <x-slot name="content">
      <div>
        <div>
          <x-jet-label value="Nombre" />
          <x-jet-input class="block mt-1 w-full" type="text" wire:model="prodName" readonly />
          @error('prodName') <span class="error" style="color: red;">{{ $message }}</span> @enderror
        </div>

        <div class="mt-4">
          <x-jet-label value="Agregar Unidades en inventario" />
          <x-jet-input class="block mt-1 w-full" wire:model="prodStock" type="text" />
          @error('prodStock') <span class="error" style="color: red;">{{ $message }}</span> @enderror
        </div>
      </div>
    </x-slot>

    <x-slot name="footer">
        <x-jet-secondary-button wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
            Cancelar
        </x-jet-secondary-button>

        <x-jet-button class="ml-2" wire:click="updateStock" wire:loading.attr="disabled">
          Guardar
        </x-jet-danger-button>
    </x-slot>
</x-jet-confirmation-modal>
</div>
