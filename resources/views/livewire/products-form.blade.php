<div>
  <x-app-layout>
    <x-slot name="header">
      <div class="inline-flex justify-between w-full">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          <a href="{{ route('productos') }}">
            Productos
          </a>
        </h2>
      @if ($product->id)
        <div>
          <button wire:click="destroy" class="bg-transparent hover:bg-blue-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: red; border-color: red;">
            Eliminar
          </button>
          <a
            class="bg-transparent hover:bg-blue-500 mr-5 text-red-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" style="color: green; border-color: green;"
            href="{{ route('productos.new') }}">
            Nuevo
          </a>
        </div>
      @endif
      </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <form wire:submit.prevent="save" class="py-10 px-4">
                <div>
                    <x-jet-label value="Nombre" />
                    <x-jet-input class="block mt-1 w-full" type="text" wire:model="product.name" autofocus />
                    @error('product.name') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                    <x-jet-label value="Precio de compra" />
                    <x-jet-input class="block mt-1 w-full" wire:model="product.bought_price" type="text" />
                    @error('product.bought_price') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Precio de venta" />
                  <x-jet-input class="block mt-1 w-full" wire:model="product.sell_price" type="text" />
                  @error('product.sell_price') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Unidades en inventario" />
                  <x-jet-input class="block mt-1 w-full" wire:model="product.stock" type="text" />
                  @error('product.stock') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Estado" />
                  <select class="block mt-1 w-full" wire:model="product.state">
                    <option value="Activo">Activo</option>
                    <option value="Inactivo">Inactivo</option>
                  </select>

                  @error('product.state') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Proveedor" />
                  <x-jet-input class="block mt-1 w-full" wire:model="product.provider" type="text" />
                  @error('product.provider') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4">
                  <x-jet-label value="Bodega" />
                  <select class="block mt-1 w-full" wire:model="product.cellar_id">
                    @foreach ($types as $itype)
                      <option value="{{ $itype->id }}">{{ $itype->name }}</option>
                    @endforeach
                  </select>
                  @error('product.cellar_id') <span class="error" style="color: red;">{{ $message }}</span> @enderror
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-jet-button class="ml-4">
                        Guardar
                    </x-jet-button>
                </div>
              </form>
            </div>
        </div>
    </div>
  </x-app-layout>
</div>
