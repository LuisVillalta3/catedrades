@isset($id)
  @livewire('products-form', ['id' => $id])
@else
  @livewire('products-form')
@endisset
