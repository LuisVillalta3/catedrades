@isset($id)
  @livewire('bills-form', ['id' => $id])
@else
  @livewire('bills-form')
@endisset
