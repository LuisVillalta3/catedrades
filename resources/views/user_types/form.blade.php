@isset($id)
  @livewire('user-types-form', ['id' => $id])
@else
  @livewire('user-types-form')
@endisset
