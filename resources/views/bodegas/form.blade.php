@isset($id)
  @livewire('cellars-form', ['id' => $id])
@else
  @livewire('cellars-form')
@endisset
