<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  use HasFactory;
  protected $fillable = [
    'name',
    'bought_price',
    'sell_price',
    'stock',
    'state',
    'provider',
    'cellar_id'
  ];

  public function cellar()
  {
    return $this->belongsTo(Cellar::class);
  }
}
