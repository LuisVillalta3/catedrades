<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cellar extends Model
{
  use HasFactory;

  protected $fillable = [
    'name',
    'ubication'
  ];

  public function products()
  {
    return $this->hasMany(Product::class);
  }
}
