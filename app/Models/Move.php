<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
  use HasFactory;

  protected $fillable = [
    'bill_id',
    'product_id',
    'qty'
  ];

  public function bill()
  {
    return $this->belongsTo(Bill::class);
  }

  public function product()
  {
    return $this->belongsTo(Product::class);
  }
}
