<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
  use HasFactory;

  protected $fillable = [
    'type',
    'total',
    'total_iva',
    'client',
    'user_id'
  ];

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function moves()
  {
      return $this->hasMany(Move::class);
  }
}
