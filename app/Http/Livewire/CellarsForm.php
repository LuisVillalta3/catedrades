<?php

namespace App\Http\Livewire;

use App\Models\Cellar;
use Livewire\Component;

class CellarsForm extends Component
{
  public $itemid;
  public $name;
  public $ubication;

  public function mount($id = null)
  {
    if ($id) {
      $item = Cellar::find($id);
      $this->itemid = $item->id;
      $this->name = $item->name;
      $this->ubication = $item->ubication;
    }
  }

  public function render()
  {
    return view('livewire.cellars-form');
  }

  public function save()
  {
    $validatedData = $this->validate([
      'name'  =>  'required',
      'ubication' =>  'required'
    ]);

    if ($this->itemid) {
      $item = Cellar::find($this->itemid);
      $item->update($validatedData);
    } else {
      $item = Cellar::create($validatedData);
    }

    return redirect()->route('bodegas.edit', $item->id);
  }

  public function destroy()
  {
    if ($this->itemid) {
      Cellar::where('id', $this->itemid)->delete();
      return redirect()->route('bodegas');
    }
  }
}
