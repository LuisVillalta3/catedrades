<?php

namespace App\Http\Livewire;

use App\Models\Bill;
use App\Models\Move;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class BillsForm extends Component
{
  public Bill $product;
  public $repeat = 0;
  public $details;
  public $products;
  public $total;

  protected $rules = [
    'product.client'  =>  'required',
    'product.type'  =>  'required',
    // 'product.total'  =>  'required|numeric'
  ];

  public function mount($id = null)
  {
    $this->products = Product::all();
    if ($id) {
      $this->product = Bill::find($id);
      $this->details = $this->product->moves;
    } else {
      $this->product = new Bill();
      $this->details = [];
    }
  }

  public function render()
  {
    $this->total = 0;
    return view('livewire.bills-form');
  }

  public function save()
  {
    $this->validate();

    // dd($this->details);

    $this->product->total = $this->calctotal();
    $this->product->total_iva = $this->product->total * 1.13;
    $this->product->user_id = Auth::id();
    $this->product->save();

    foreach ($this->details as $item) {
      $detail = new Move();
      $p = Product::where('id', $item['product_id'])->first();
      $detail->product_id = $p->id;
      $detail->qty = $item['qty'];
      $detail->bill_id = $this->product->id;

      $detail->save();
      // dd($detail);
    }

    return redirect()->route('facturas.edit', $this->product->id);
  }

  public function addProduct()
  {
    $this->repeat++;
    $this->details[] = ['id' => null ,'product_id' => Product::first(), 'qty' => 1];
  }

  public function calctotal()
  {
    $otal = 0;
    foreach ($this->details as $item) {
      $p = Product::where('id', $item['product_id'])->first();
      $otal += $item['qty'] * $p->sell_price;
    }
    return $otal;
  }
}
