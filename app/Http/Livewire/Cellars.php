<?php

namespace App\Http\Livewire;

use App\Models\Cellar;
use Livewire\Component;

class Cellars extends Component
{
  public $name = '';

  public function render()
  {
    $searchable_name = '%' . $this->name . '%';
    #
    return view('livewire.cellars', [
      'users' => Cellar::where('name', 'like', $searchable_name)->paginate(7)
    ]); 
  }

  public function destroy($id)
  {
    if ($id) {
      Cellar::where('id', $id)->delete();
    }
  }
}
