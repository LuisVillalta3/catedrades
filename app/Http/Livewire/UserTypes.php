<?php

namespace App\Http\Livewire;

use App\Models\UserType;
use Livewire\Component;

class UserTypes extends Component
{
  public $name = '';

  public function render()
  {
    $searchable_name = '%' . $this->name . '%';
    #
    return view('livewire.user-types', [
      'users' => UserType::where('name', 'like', $searchable_name)->paginate(7)
    ]);
  }

  public function destroy($id)
  {
    if ($id) {
      UserType::where('id', $id)->delete();
    }
  }
}
