<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class Products extends Component
{
  public $name = '';
  public $confirmingUserDeletion = false;
  public Product $product;
  public $prodName;
  public $prodStock;

  public function mount()
  {
    $this->product = new Product();
  }

  public function render()
  {
    $searchable_name = '%' . $this->name . '%';
    return view('livewire.products', [
      'users' => Product::where('name', 'like', $searchable_name)->paginate(10)
    ]);
  }

  public function destroy($id)
  {
    if ($id) {
      Product::where('id', $id)->delete();
    }
  }

  public function showupdateStock($id)
  {
    $this->product = Product::find($id);
    $this->prodName = $this->product->name;
    $this->confirmingUserDeletion = true;
  }

  public function updateStock()
  {
    $this->product->stock += $this->prodStock;
    $this->product->save();

    $this->product = new Product();
    $this->prodStock = null;
    $this->prodName = null;
    $this->confirmingUserDeletion = false;
  }
}
