<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\Component;

class UsersForm extends Component
{
  public $itemid;
  public $name;
  public $lastname;
  public $username;
  public $email;
  public $password;
  public $user_type_id;
  public $types;

  public function mount($id = null)
  {
    $this->types = UserType::all();
    if ($id) {
      $item = User::find($id);
      $this->itemid = $item->id;
      $this->name = $item->name;
      $this->lastname = $item->lastname;
      $this->username = $item->username;
      $this->email = $item->email;
      $this->user_type_id = $item->user_type_id;
    }
  }

  public function render()
  {
    return view('livewire.users-form');
  }

  public function save()
  {
    if ($this->itemid) {
      $validatedData = $this->validate([
        'name'  =>  'required',
        'lastname' =>  'required',
        'username' =>  'required',
        'email' =>  [
          'required',
          'email',
          Rule::unique('users')->ignore($this->itemid, 'id')],
        'user_type_id'    =>  'required'
      ]);
    } else {
      $validatedData = $this->validate([
        'name'  =>  'required',
        'lastname' =>  'required',
        'username' =>  'required',
        'email' =>  'required|email|unique:users',
        'password' =>  'required',
        'user_type_id'    =>  'required'
      ]);
    }

    if ($this->itemid) {
      $item = User::find($this->itemid);
      $item->update($validatedData);
      $item->update([
        'password' => Hash::make($this->password)
      ]);
    } else {
      $item = User::create($validatedData);
      $item->update([
        'password' => Hash::make($this->password)
      ]);
    }

    return redirect()->route('usuarios.edit', $item->id);
  }

  public function destroy()
  {
    if ($this->itemid && Auth::id() != $this->itemid) {
      User::where('id', $this->itemid)->delete();
      return redirect()->route('usuarios');
    }
  }
}
