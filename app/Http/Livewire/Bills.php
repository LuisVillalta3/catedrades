<?php

namespace App\Http\Livewire;

use App\Models\Bill;
use Livewire\Component;

class Bills extends Component
{
  public $name = '';

  public function render()
  {
    $searchable_name = '%' . $this->name . '%';
    return view('livewire.bills', [
      'users' => Bill::where('client', 'like', $searchable_name)->paginate(10)
    ]);
  }

  public function destroy($id)
  {
    if ($id) {
      Bill::where('id', $id)->delete();
    }
  }
}
