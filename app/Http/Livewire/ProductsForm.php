<?php

namespace App\Http\Livewire;

use App\Models\Cellar;
use App\Models\Product;
use Livewire\Component;

class ProductsForm extends Component
{
  public Product $product;
  public $types;

  protected $rules = [
    'product.name'  =>  'required',
    'product.stock'  =>  'required|numeric|integer',
    'product.bought_price'  =>  'required|numeric',
    'product.sell_price'  =>  'required|numeric',
    'product.stock'  =>  'required',
    'product.provider'  =>  'required'
  ];

  public function mount($id = null)
  {
    $this->types = Cellar::all();
    if ($id) {
      $this->product = Product::find($id);
    } else {
      $this->product = new Product();
    }
  }

  public function render()
  {
    return view('livewire.products-form');
  }

  public function save()
  {
    $this->validate();

    $this->product->save();

    return redirect()->route('productos.edit', $this->product->id);
  }

  public function destroy()
  {
    if ($this->product->id) {
      Product::where('id', $this->product->id)->delete();
      return redirect()->route('productos');
    }
  }
}
