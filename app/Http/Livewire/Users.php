<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class Users extends Component
{

  public $name = '';

  public function render()
  {
    $searchable_name = '%' . $this->name . '%';
    #
    return view('livewire.users', [
      'users' => User::where('name', 'like', $searchable_name)->paginate(10)
    ]);
  }

  public function destroy($id)
  {
    if ($id) {
      User::where('id', $id)->delete();
    }
  }
}
