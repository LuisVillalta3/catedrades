<?php

namespace App\Http\Livewire;

use App\Models\UserType;
use Livewire\Component;

class UserTypesForm extends Component
{
  public $itemid;
  public $name;
  public $description;

  public function mount($id = null)
  {
    if ($id) {
      $item = UserType::find($id);
      $this->itemid = $item->id;
      $this->name = $item->name;
      $this->description = $item->description;
    }
  }

  public function render()
  {
    return view('livewire.user-types-form');
  }

  public function save()
  {
    $validatedData = $this->validate([
      'name'  =>  'required',
      'description' =>  'required'
    ]);

    if ($this->itemid) {
      $item = UserType::find($this->itemid);
      $item->update($validatedData);
    } else {
      $item = UserType::create($validatedData);
    }

    return redirect()->route('tiposusuarios.edit', $item->id);
  }

  public function destroy()
  {
    if ($this->itemid) {
      UserType::where('id', $this->itemid)->delete();
      return redirect()->route('tiposusuarios');
    }
  }
}
