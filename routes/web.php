<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/usuarios', function () {
  return view('users.index');
})->name('usuarios');

Route::middleware(['auth:sanctum', 'verified'])->get('/usuarios/nuevo', function () {
  return view('users.form');
})->name('usuarios.new');

Route::middleware(['auth:sanctum', 'verified'])->get('/usuarios/editar/{id}', function ($id) {
  return view('users.form', compact('id'));
})->name('usuarios.edit');


Route::middleware(['auth:sanctum', 'verified'])->get('/tipos-usuarios', function () {
  return view('user_types.index');
})->name('tiposusuarios');

Route::middleware(['auth:sanctum', 'verified'])->get('/tipos-usuarios/nuevo', function () {
  return view('user_types.form');
})->name('tiposusuarios.new');

Route::middleware(['auth:sanctum', 'verified'])->get('/tipos-usuarios/editar/{id}', function ($id) {
  return view('user_types.form', compact('id'));
})->name('tiposusuarios.edit');

Route::middleware(['auth:sanctum', 'verified'])->get('/bodegas', function () {
  return view('bodegas.index');
})->name('bodegas');

Route::middleware(['auth:sanctum', 'verified'])->get('/bodegas/nuevo', function () {
  return view('bodegas.form');
})->name('bodegas.new');

Route::middleware(['auth:sanctum', 'verified'])->get('/bodegas/editar/{id}', function ($id) {
  return view('bodegas.form', compact('id'));
})->name('bodegas.edit');

Route::middleware(['auth:sanctum', 'verified'])->get('/productos', function () {
  return view('productos.index');
})->name('productos');

Route::middleware(['auth:sanctum', 'verified'])->get('/productos/nuevo', function () {
  return view('productos.form');
})->name('productos.new');

Route::middleware(['auth:sanctum', 'verified'])->get('/productos/editar/{id}', function ($id) {
  return view('productos.form', compact('id'));
})->name('productos.edit');

Route::middleware(['auth:sanctum', 'verified'])->get('/facturas', function () {
  return view('facturas.index');
})->name('facturas');

Route::middleware(['auth:sanctum', 'verified'])->get('/facturas/nuevo', function () {
  return view('facturas.form');
})->name('facturas.new');

Route::middleware(['auth:sanctum', 'verified'])->get('/facturas/ver/{id}', function ($id) {
  return view('facturas.form', compact('id'));
})->name('facturas.edit');
