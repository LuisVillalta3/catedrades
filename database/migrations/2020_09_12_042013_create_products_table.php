<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function (Blueprint $table) {
      $table->id();
      $table->string('name');
      $table->decimal('bought_price', 60, 2);
      $table->decimal('sell_price', 60, 2);
      $table->integer('stock')->unsigned()->nullable()->default(0);
      $table->string('state')->nullable()->default('Activo');
      $table->string('provider')->nullable()->default('');
      $table->bigInteger('cellar_id')->unsigned()->nullable()->default(1);
      $table->foreign('cellar_id')->references('id')->on('cellars')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('products');
  }
}
