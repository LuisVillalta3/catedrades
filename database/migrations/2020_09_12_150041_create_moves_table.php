<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('moves', function (Blueprint $table) {
      $table->id();
      $table->bigInteger('bill_id')->unsigned();
      $table->bigInteger('product_id')->unsigned();
      $table->integer('qty')->unsigned()->nullable()->default();
      $table->foreign('bill_id')->references('id')->on('bills')->onDelete('cascade');
      $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('moves');
  }
}
