<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
          'name' => $this->faker->name,
          'bought_price' => $this->faker->numerify,
          'sell_price' => $this->faker->numerify,
          'stock' => $this->faker->numberBetween(1, 1000),
          'state' => 'Activo',
          'provider' => $this->faker->name,
          'cellar_id' => 1
        ];
    }
}
