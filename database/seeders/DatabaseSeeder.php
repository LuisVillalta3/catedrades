<?php

namespace Database\Seeders;

use App\Models\Cellar;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      // User::factory(20)->create();
      // Cellar::factory(14)->create();
      Product::factory(20)->create();
    }
}
